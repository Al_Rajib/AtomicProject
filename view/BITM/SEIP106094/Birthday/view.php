<?php
session_start();
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");
use App\BITM\SEIP106094\Birthday;
use App\BITM\SEIP106094\message\Message;

$obj=new Birthday();
$b=$obj->view($_GET['id']);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<h1><?php echo $b->name;?></h1>
<dl>
    <dt>Birth Day</dt>
    <dd><?php echo $b->bdate;?></dd>
</dl>
<nav>
    <li><a href="index.php">Go to list</a></li>
</nav>
</body>
</html>
