<?php session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");
use App\BITM\SEIP106094\Birthday;
use App\BITM\SEIP106094\message\Message;
$obj=new Birthday();
$_birth=$obj->index();

?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>List of Birthday</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<h1>List of Birthday</h1>
<div class="warning">
    <?php
    echo Message::flash();
    ?>
</div>
<div>
    <span>Search /Filter </span>
    <span id="utility">Download as PDF | XL <a href="create.php">Create New</a> </span>
    <select>
        <option>10</option>
        <option>20</option>
        <option>30</option>
        <option>40</option>
        <option>50</option>
    </select>
</div>
<table border="1">
    <thead>
    <tr>
        <th>Sl</th>
        <th>Name</th>
        <th>Birthday &dArr;</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
<?php
$slno=0;
foreach($_birth as $_birthday):
   $slno++;
?>
    <tr>
        <td><?php echo $slno;?></td>
        <td><a href="view.php?id=<?php echo $_birthday['id']?>"><?php echo $_birthday['name'];?></a></td>
        <td><?php echo $_birthday['bdate'];?></td>
        <td><a href="edit.php?id=<?php echo $_birthday['id']?>"> Edit</a> |
            <form action="delete.php" method="post">
                <input type="hidden" name="id" value="<?php echo $_birthday['id'];?>">
                <button type="submit">Delete</button>
            </form>
             |Trash/Recover | Email To Friend</td>
    </tr>

<?php endforeach;?>
    </tbody>
</table>
<div><span>prev 1 | 2 | 3 next </span></div>
</body>
</html>